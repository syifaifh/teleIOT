<?php

namespace App\Widgets;

use DB;
use Arrilot\Widgets\AbstractWidget;

class recent extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $tinggi_air = DB::table('tinggi_air')->latest("id")->get();

        return view('widgets.recent', [
            'config' => $this->config,
            'tinngi_air' => $tinggi_air_recent 
        ]);
    }
}
