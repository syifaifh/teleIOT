<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/store/{tinggi_air}', 'iotController@tinggi_air');
Route::post('/last','iotController@last');
Route::get('/last/{tinggi_air}','iotController@last');
Route::get('/','iotController@index');
Route::get('/recent', function () {
    return view('widgets.recent');
});
Route::get('/chart/{type}','iotController@chart');
Route::get('stock/add','StockController@create');
Route::post('stock/add','StockController@store');
Route::get('chartjs', 'iotController@chartjs');